export default () => {
  let buttons = $('.show-more-button'),
      containers = $('.show-more-container');
  for(let i = 0; i < containers.length; i++){
    let container  = containers.eq(i),
        box = $('<div class="box"></div>'),
        items = container.children(),
        startCount = container.data('show-start'),
        height = items.eq(startCount-1).offset().top + items.eq(startCount-1).height() - container.offset().top;
    
    container.wrap(box);
    container.parent().height( height );
  }
  buttons.on('click', function (){
    let $this = $(this),
        openedText = $this.data('opened-text'),
        closedText = $this.data('closed-text'),
        box = $this.prev(),
        container = box.children(),
        items = container.children(),
        startCount = container.data('show-start'),
        height = items.eq(startCount-1).offset().top + items.eq(startCount-1).height() - container.offset().top;

    if($this.hasClass('show-more-button_opened')){
      $this.text(closedText);
      $this.removeClass('show-more-button_opened');
      box.animate({
        height: height
      },1000);  
    }else{
      $this.text(openedText)
      $this.addClass('show-more-button_opened');
      box.animate({
        height: container.height()
      },1000);   
    }
  });
}