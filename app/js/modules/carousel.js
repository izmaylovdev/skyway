import CarouselPorgressBar from './CarouselProgressBar';
import { Subject } from "rxjs/Subject";

export default (resizeStream) => {
  let carousels = $('.carousel');

  for(let i=0; i < carousels.length; i++){ 
    let progressBar = carousels.eq(i).next();
    let carousel = new Carousel(carousels.eq(i), resizeStream);
        
    if(progressBar.hasClass('carousel-progress')){
      new CarouselPorgressBar(resizeStream, progressBar, carousel);
    }
  }
}
class Carousel {
  
  constructor(carousel, resizeStream) {
    this.resizeStream = resizeStream;
    this.carousel = carousel;
    this.isSmall = carousel.hasClass('carousel_small');
    this.current = 0;
    this.count;
    this.itemsList = carousel.find('.carousel__items');
    this.items = this.itemsList.find('.carousel__item');
    this.prev = null;
    this.next = null;

    this.marginLeft = parseInt(this.items.eq(2).css('margin-left'));
    this.itemCount = this.items.length;
    this.controls = carousel.find('.carousel__next, .carousel__prev');
    this.itemWidth;
    this.slideStream = new Subject();

    this.items.css('margin-left', this.marginLeft+'px');
    this.items.eq(0).css('margin-left', 0);

    this.setUpListeners();
    this.resize();
  }

  setPrev(item){
    for(let i = 0; i < this.items.index(item); i++){
      this.items.eq(i).addClass('carousel__item_opacity');
    }
    if(this.prev){
      this.prev.removeClass('carousel__item_opacity');
    }
    this.prev = item;
    if(item){
      item.addClass('carousel__item_opacity');
    }
  }
  setNext(item){
    let index = this.items.index(item);
    if(index !== -1)
      for(let i = index; i < this.items.length - 1; i++){
        this.items.eq(i).addClass('carousel__item_opacity');
      }
    if(this.next){
      this.next.removeClass('carousel__item_opacity');
    }
    this.next = item;
    if(item){
      item.addClass('carousel__item_opacity');
    }
  }
  resize () {
    let ww = window.innerWidth;    
    this.marginLeft = parseInt(this.items.eq(2).css('margin-left'));
    if(this.isSmall){
      this.count =  ww > 1024? 5: ww > 768? 4: ww > 600? 3: ww > 375? 2: 1;
    } else{
      this.count = ww > 1024? 4: ww > 768? 2: ww > 500? 2: 1;
    }
    this.itemWidth = (this.carousel.width() - (this.count+1)*this.marginLeft)/(this.count+1);
    this.items.css({
      width: this.itemWidth
    });
    this.itemsList.css({
      width: this.itemCount * (this.marginLeft+this.itemWidth) + 100
    });
    this.controls.css('width', this.itemWidth/2+1);
    this.slideTo(this.current);
  }

  slideTo(position){
    let prev = this.controls.filter('.carousel__prev'),
        next = this.controls.filter('.carousel__next'),
        left;
    
    if(position === 0){
      prev.hide();
      left = this.itemWidth/2 + this.marginLeft;
    } else {
      prev.show();    
      left = -(this.itemWidth)*(position-.5) - this.marginLeft*(position-1);
    }

    if(position < this.items.length-this.count){
      next.show();
    } else{
      next.hide();
    }

    this.itemsList.stop(true,false).animate({
      left: left
    },500);

    this.slideStream.next(this);
    this.setPrev(this.current? this.items.eq(this.current-1) : null);
    this.setNext(this.current != this.items.length-1 ? this.items.eq(this.current+this.count) :  null);
  }

  setUpListeners (){
    let next = Observable.fromEvent(this.controls.filter('.carousel__next'), 'click');
    let prev = Observable.fromEvent(this.controls.filter('.carousel__prev'), 'click');

    next.subscribe((event) => {
      if( this.current < this.carousel.find('.carousel__item').length-this.count ){
        this.slideTo(++this.current);
      } 
      event.preventDefault();
    });

    prev.subscribe((event) => {
      if(this.current > 0){
        this.slideTo(--this.current);
      }
      event.preventDefault();
    });
    
    this.resizeStream.subscribe((event) => {
      this.resize();
    });
  }
}