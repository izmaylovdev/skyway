export default (resizeStream) => {
  let func = () => {
    let paddingBlocks = $('.progress__bar__item_big'),
        icon = $('.time-line'),
        parentWidth = paddingBlocks.parent().parent()[0].offsetWidth,
        paddings = paddingBlocks[0].offsetWidth + paddingBlocks[1].offsetWidth;
    icon.css({
      left: paddingBlocks[0].offsetWidth - 3,
      width: parentWidth - paddings + 5
    });

    let years = $('.progress__legend__bottom span');
    years.eq(1).css({
      left: paddingBlocks[0].offsetWidth - years[1].offsetWidth/2
    });
    years.eq(2).css({
      left: parentWidth - paddingBlocks[1].offsetWidth - years[2].offsetWidth/2
    });
  }
  resizeStream.subscribe(func);
  func();
}