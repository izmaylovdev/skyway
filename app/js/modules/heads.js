export default (scrollStream) => {
  const heads = $('.how-it-works__img_rotate');
  if(heads.length){
      let deg = -15 * -(heads.offset().top - (document.body.scrollTop + window.innerHeight))/(window.innerHeight + heads.height());
      heads
        .stop(true, true)
        .css({
          transform: `rotateZ(${55 + deg }deg)`
        });
    heads.css('transition','.3s');
    scrollStream
      .filter((bottomOfWindow) => {
        let isOnScreen = heads.offset().top < bottomOfWindow && heads.offset().top + heads.height() > document.body.scrollTop;
        return isOnScreen;
      })
      .subscribe((bottomOfWindow) => {
        let deg = -15 * -(heads.offset().top - bottomOfWindow)/(window.innerHeight + heads.height());
        heads
          .stop(true, false)
          .css({
            transform: `rotateZ(${55 + deg }deg)`
          });
      })
  }
}