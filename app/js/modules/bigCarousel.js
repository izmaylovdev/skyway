export default (resizeStream, scrollStream) => {
  let carousels = document.getElementsByClassName('slider_big');

  for(let i=0; i< carousels.length; i++){
    let slider = new BigSlider(carousels[i]);
  }
  scrollStream.subscribe(() => {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if( scrollTop > 50 )
      $('.slider__controls').stop(true).css('opacity', 0);
    else
      $('.slider__controls').stop(true).css('opacity', 1);
  })
}

  class BigSlider  {

    constructor (carousel) {
      this.carousel = $(carousel);
      this.slides = this.carousel.find('.slider__slide');
      this.dataView = this.carousel.find('.slider__controls-data');
      this.slidesCount = this.slides.length;
      this.next = this.carousel.find('.slider__next');
      this.prev = this.carousel.find('.slider__prev');
      this.active = 0;
      this.data = `1/${this.slidesCount}`;
      this.carousel.height(this.slides.eq(0).height());
      this.setUpListenres();
      this.setData(0);
    }

    setData(index){
      this.data = `${index+1}/${this.slidesCount}`;
      this.dataView.text(this.data);
    }

    goPrev() {
      let prevEl = this.active === 0? this.slides.eq(this.slides.length - 1): this.slides.eq(this.active - 1),
          activeEl = this.slides.eq(this.active);

      this.active = this.active === 0 ? this.slides.length-1 : this.active-1;
      this.setData(this.active);
      prevEl.css({
        left: -document.body.offsetWidth/3
      })      
      prevEl.stop(true,false).animate({
        left: 0,
        'z-index': 1
      },1000);
      prevEl.addClass('slider__slide_active');

      activeEl.stop(true,false).animate({
        left: document.body.offsetWidth,
        'z-index': 0
      }, 1000, ()=>{
        activeEl.removeClass('slider__slide_active');
      });
    }

    goNext () {
      let nextEl = this.active === this.slides.length - 1 ? this.slides.eq(0): this.slides.eq(this.active + 1),
          activeEl = this.slides.eq(this.active);

      this.active = this.active === this.slides.length - 1 ? 0: this.active+1;

      this.setData(this.active);
      nextEl.css({
        left: document.body.offsetWidth,
        'z-index': 1
      })      
      nextEl.animate({
        left: 0,
        'z-index': 1
      },1000);
      nextEl.addClass('slider__slide_active');

      activeEl.css('z-index',0);
      activeEl.animate({
        left: -document.body.offsetWidth/3,
      }, 1000, ()=>{
        activeEl.removeClass('slider__slide_active');
      });
    }

    setUpListenres(){
      let that = this;
      this.prev.on('click', (event) => {
        that.goPrev();
        event.preventDefault();
      })
      this.next.on('click', (event) => {
        that.goNext();
        event.preventDefault();
      })
    }
  }
