export default (resizeStream) => {
  let tabs = $('.tabs');

  let init = () => {
    for (var i = 0; i < tabs.length; i++) {
      var activeTab = tabs.eq(i).find('.tabs__item_active'),
          list = tabs.eq(i).find('.tabs__list');
      list.height(activeTab.height());
    }
  }
  resizeStream.subscribe(init);

  tabs.find('.dots__link').on('click', function (event){
    let $this = $(this),
        dotsItem = $this.parent(),
        index = dotsItem.parent().children().index(dotsItem);


    dotsItem
      .siblings()
      .children()
      .removeClass('dots__link_active');

    $this.addClass('dots__link_active');

    let tabs = dotsItem.closest('.tabs'),
        list = tabs.find('.tabs__list'),
        tabsItems = list.children(),
        activeTab = tabsItems.find('tabs__item_active');

    tabsItems.find('.tabs__img').removeClass('active');
    tabsItems.eq(index).find('.tabs__img').addClass('active');

    tabsItems.stop(true,false).animate({
      opacity: 0,
      'z-index': 0
    }, 500, () => { tabsItems.removeClass('tabs__item_active');});

    tabsItems.eq(index).stop(true,false).animate({
      opacity: 1,
      'z-index': 1,
    }, 500, () => { tabsItems.eq(index).addClass('tabs__item_active'); });
    

    list.animate({
      height: tabsItems.eq(index).height()
    }, 500, ()=>{list.css('height','auto')});
    
    event.preventDefault();
  })
  
  init();
  for (var i = 0; i < tabs.length; i++) {
    tabs.eq(i).find('.dots__link').eq(0).click();
  }
  
}