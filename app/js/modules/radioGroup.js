export default () => {
  const groups = $('.news__form__radio-group');

  for (var i = 0; i < groups.length; i++) {
    var group = groups.eq(i),
        items = group.find('.news__form__label'),
        height = items.height() * items.length;

    group[0].setAttribute('data-height', height);
    group.height(items.height()-2);
    items.wrapAll('<div class="wrap"></div>');
  }

  const open = function (event){
    const $this = $(this);
    $this.addClass('active');
    let items = $(this).find('.news__form__label'),
        height = $this.data('height'),
        wrap = $this.find('.wrap');
    $this.css({ height });
    wrap.css({ marginTop: 0 });
  }
  groups.on('mouseenter', open);

  const close = function (event){
    const $this = $(this);
    let items = $(this).find('.news__form__label'),
        height = items.height(),
        active = $this.find('input:checked').parent(),
        activeIndex = items.index(active),
        wrap = $this.find('.wrap'),
        marginTop = -(activeIndex*height);

    $this.css({ height });
    wrap.css({ marginTop });
    $this.removeClass('active');
    
  }
  groups.on('mouseleave', close);

  groups.on('click', function (){
    const $this = $(this);
    let items = $(this).find('.news__form__label'),
        height = items.height();

    console.log(this.offsetHeight, items.height());
    if( this.offsetHeight === items.height() ){
      open.apply(this, [event]);
    }else{
        groups.trigger('mouseleave');
    }
  });
}