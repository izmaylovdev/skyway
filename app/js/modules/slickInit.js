export default () => {
    $('.slider_default .slider__slides').slick({
      prevArrow: $('.slider_default .slider__prev'),
      nextArrow: $('.slider_default .slider__next'),
      speed: 1000,
      infinite: false
    });
    $('.progress__bar__item').click(function (event) {
      let index = $('.progress__bar__item').index(this);
      $('.slider_default .slider__slides').slick('slickGoTo', index);
    });

    $('.slider_default .slider__slides').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      $('.progress__bar__item').eq(currentSlide).removeClass('progress__bar__item_active');
      $('.progress__bar__item').eq(nextSlide).addClass('progress__bar__item_active');
    });
    $('.slider_default .slider__slides').slick('slickGoTo', 9);
    let productSlider = $('.products__slider .slider__slides');
    productSlider.slick({
      prevArrow: $('.products__slider .slider__prev'),
      nextArrow: $('.products__slider .slider__next'),
      speed: 1000
    }	);
    $('.products__slider__dots-link').click(function (event) {
      let index = $('.products__slider__dots-link').index(this);
      productSlider.slick('slickGoTo', index);
      event.preventDefault();
    });
    productSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
      let dots = $('.products__slider__dots-link');

      dots.eq(currentSlide).removeClass('products__slider__dots-link_active');
      dots.eq(nextSlide).addClass('products__slider__dots-link_active');
    });

    productSlider.slick('slickGoTo', 0);

    $('.banner').slick({
      fade: true,
      arrows: false,
      slidesToShow: 1,
      autoplay: 0,
      autoplaySpeed: 3000,
      dots: true,
      dotsClass: 'banner__dots'
    })
}