export default () => {
  let frame = document.getElementById('frame');

  if(frame != undefined){
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";

    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    window.onYouTubeIframeAPIReady = () => {
      let cover = frame.nextElementSibling;
          
      player = new YT.Player('frame', {
        height: frame.getAttribute('data-height'),
        width: frame.getAttribute('data-width'),
        videoId: frame.getAttribute('data-key'),
        events: {
        }
      });
      cover.addEventListener('click', () => {
        player.playVideo(),
        cover.style.display = 'none';
      })
    }
  }

}