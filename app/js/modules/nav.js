export default (resize) => {
  $('.nav__burger-link').click(function (event){
    $(this).siblings().stop(true,false).fadeToggle(500);
    $(this).parent().toggleClass('nav_opened');
  })
  $('.nav__cover').click(function (){
    $(this).siblings('.nav__burger-link').click();
  });
  resize.subscribe(() => {
    if( document.body.offsetWidth > 1279 ){
      $('.nav__list').css('display', 'flex');
    }
    else {
      $('.nav__list').css('display', 'none');
      $('.nav__cover').css('display', 'none');
      $('.nav').removeClass('nav_opened');
    }
  });
}
