export default () => {
$('.localization__item_active').click( event => {
	let localization = $('.localization');
	localization.stop(true,false).slideToggle(300);
	localization.toggleClass('localization_opened');
});
$('.localization__item:not(.localization__item_active)').on('mouseenter', function(event) {
	$(this).siblings()
		.stop(true,false)
		.animate({
			opacity: .6
		},300)
});
$('.localization__item:not(.localization__item_active)').on('mouseleave', function(event) {
	$(this).siblings()
		.stop(true,false)
		.animate({
			opacity: 1
		},300)
})

}