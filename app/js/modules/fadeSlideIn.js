export default (scrollStream) => {
    let elements = document.getElementsByClassName('fsi');

    for(let index=0; index < elements.length; index++ ) {     
			let	element = elements[index],
					elTop = element.getBoundingClientRect().top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
					$el = $(element),
					animateStartTop = elTop + element.offsetHeight/2;
				
		let subscribe = scrollStream
			.subscribe((bottomOfWindow) => {	
				if( bottomOfWindow > animateStartTop){
					$el.addClass("fsi_active");
					unsubscribe();
				}
			});
		let unsubscribe = () => {
			subscribe.unsubscribe();
		}
    }
}