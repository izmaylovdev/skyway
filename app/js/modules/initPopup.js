export default () => {
  	$('.gallery').each(function(){
		$(this).magnificPopup({
			delegate: 'img',
			zoom: {
				enabled: true,
				duration: 400
			},
			gallery: {
				enabled: true
			},
			type: 'image',
			callbacks: {
				elementParse: (item) => { item.src = item.el.attr('data-src') }
				
			}
		})
	});
	$('.named-photos').each(function(){
		$(this).magnificPopup({
			delegate: '.named-photo',
			gallery: {
				enabled: true
			},
			type: 'image',
			callbacks: {
				elementParse: (item) => { item.src = item.el.find('img').attr('src') }
			}
		})
	});	
	$('.documents').each(function(){
		$(this).magnificPopup({
			delegate: 'img',
			gallery: {
				enabled: true
			},
			type: 'image',
			callbacks: {
				elementParse: (item) => { item.src = item.el.attr('src') }
			}
		})
	});
}