export default () => {
  let tooltipsList = $('.tooltips');

  for (let i = 0; i < tooltipsList.length; i++) {
    let tooltips = tooltipsList.eq(i),
        icons = tooltips.find('.icon-mark'),
        dots = tooltips.find('.dots__link');

    dots.on('click', function (event){
      let $this = $(this),
          dotsItem = $this.parent(),
          index = dotsItem.parent().children().index(dotsItem);

      dotsItem
        .siblings()
        .children()
        .removeClass('dots__link_active');
      $this.addClass('dots__link_active');
      event.preventDefault();
      if(dots.length && event.isTrigger === undefined){
        icons.eq(index).click();
      }
    })

    icons.on('click', function (event){
      let $this = $(this),
          index = $(this).parent().parent().children().index($(this).parent());

      if(tooltips.find('.tooltips__tooltip').eq(index).is(':visible') ){
        tooltips.find('.tooltips__tooltip, .tooltips__item .icon-tooltipArr').stop(true,false).fadeOut(500);
        dots.removeClass('dots__link_active');
      }else{
        tooltips.find('.tooltips__tooltip, .tooltips__item .icon-tooltipArr').stop(true,false).fadeOut(500);
        tooltips.find('.tooltips__item .icon-tooltipArr').eq(index).stop(true,false).fadeIn(500);
        tooltips.find('.tooltips__tooltip').eq(index).stop(true,false).fadeIn(500);

        let that = this;

          $(document).one('mousedown touchstart', function (){
            $(that).siblings('.tooltips__tooltip, .icon-tooltipArr').stop(true,false).fadeOut(500);
            dots.removeClass('dots__link_active');
          });

        if(dots.length && event.isTrigger === undefined){
          dots.eq(index).click();
        }
      }

      $(document.body)
      event.preventDefault();
    });

  }
}