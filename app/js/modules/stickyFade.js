export default (scroll) => {
    let elements = document.getElementsByClassName('stickyFade');
   for(let index=0; index < elements.length; index++ ) {     
		let	element = $(elements[index]);
        $(element).css('position', 'absolute');
        let elTop = element[0].getBoundingClientRect().top + document.body.scrollTop;
        $(element).css('position', 'fixed');
        let elFixedTop = parseInt($(element).css('top')),
            scrollTopStartSticky = elTop - elFixedTop,
            elPosition = element.css('position'),
            elDefaultTop = element.css('top'),
            animationLenght = (element.parent().height() - element.height()) * .55,
            scrollTopStartFade = elTop + animationLenght/5,
            elFadeLength =  animationLenght/3,
            scrollTopEnd = scrollTopStartFade + elFadeLength;
        let handler = (scrollTop) => {
                if( scrollTop > scrollTopStartSticky ){
                    element.css({                                
                        position: `fixed`, 
                        top: elFixedTop, 
                        transform: 'rotateX(0deg)' 
                    })
                } else{
                    element.stop().css({
                        position: elPosition, 
                        opacity: 1 , 
                        top: elDefaultTop, 
                        transform: 'rotateX(0deg)'
                    });
                }
                if( scrollTop > scrollTopStartFade ){
                    let opacity = 1 - (scrollTop - scrollTopStartFade)/elFadeLength;
                    
                    // alert(opacity + " " + elFadeLength + " " + scrollTopStartFade);
                    if(opacity > 0){
                        element.css({transorm: 'rotateX(0deg)'});
                        element.stop(true,true).animate({
                            opacity: `${ opacity }`
                        }, 100);
                    } else{
                        element.css({ transform: 'rotateX(90deg)' });
                    }
                }
            }
            handler(window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0);
        scroll
            .map((event) => {
                return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
            })
            .subscribe(handler);
    }
}