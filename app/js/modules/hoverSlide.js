export default () => {
    let items = document.getElementsByClassName('slide-hover');
    for(let index =0; index < items.length; index++) {     
        let	item = items[index],
            $item = $(item);
        item.addEventListener( 'mouseenter', (event) => {
            let el = $(event.target).find('.hover');
            if(el !== undefined){
                el.stop(true,false).css({
                    'height': item.offsetHeight,
                    bottom: -item.offsetHeight
                });
                el.animate({
                    bottom: -item.offsetHeight+3
                },300);
            }
        });
        item.addEventListener( 'mouseleave', (event) => {
            let el = $(event.target).find('.hover');
            if(el !== undefined){            
                el.animate({
                    bottom: -item.offsetHeight
                },300);
            }
        });
        $(item).on( 'click', function (event) {
            event.preventDefault();
            let el = $(this).find('.hover');
            if(el !== undefined){
                el.stop(true,false).animate({
                    bottom: item.offsetHeight
                },300, () => {
                    el.css({
                        bottom: -item.offsetHeight
                    });
                });
            }
        });
    }
}