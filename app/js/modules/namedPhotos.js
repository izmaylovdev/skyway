export default () => {
  $('.named-photo').on('mouseenter', function (){
    $(this)
      .closest('.named-photos')
      .find('.named-photo')
      .not(this)
      .addClass('named-photo_fade');
  });
  $('.named-photo').on('mouseleave', function (){
    $(this)
      .closest('.named-photos')
      .find('.named-photo')
      .not(this)
      .removeClass('named-photo_fade');
  });
}