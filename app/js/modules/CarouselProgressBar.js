export default class CarouselProgressBar{
  constructor(resizeStream, view, carousel){
    this.resizeStream = resizeStream;
    this.carousel = carousel;
    this.view = view;
    this.width = view.width();
    this.icon = view.find('.carousel-progress__progress-icon');
    this.iconWidth = this.icon.width();
  
    this.setUpListeners();
    this.resize();
  }
  slideTo (){
    let emptyWidth = this.width - this.iconWidth,
        coef = this.carousel.current/(this.carousel.itemCount-this.carousel.count),
        left = emptyWidth * coef;
    this.icon.stop(true, false).animate({'left': left}, 500);
  }
  resize (){
    this.width = this.view.width();
    this.iconWidth = this.carousel.count/this.carousel.itemCount * this.width
    this.icon.width(this.iconWidth);
    this.slideTo();
  }
  setUpListeners (){
    this.resizeStream.subscribe(event => {
     this.resize();
    });
    this.carousel.slideStream.subscribe((carousel) => {
      this.slideTo();
    });
  }
}