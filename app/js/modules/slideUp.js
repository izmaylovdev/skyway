export default (scrollStream) => {
	let elements = document.getElementsByClassName('slideUp');
	for(let index =0; index < elements.length; index++ ) {   
		let	element = elements[index],
				elTop = element.getBoundingClientRect().top + document.body.scrollTop,
				$el = $(element),
				animateStartTop = elTop + 10;
				
		let subscribe = scrollStream
				.map((event) => {
								return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
				})
				.subscribe((scrollTop) => {
						let bottomOfWindow = scrollTop +  window.innerHeight;
						if( bottomOfWindow > animateStartTop ){
							$el.addClass('slideUp_active');
							unsubscribe();
						}
				});
		let unsubscribe = () =>{
			subscribe.unsubscribe();
		}
	}
}