export default (scrollStream) => {
    let elements = document.getElementsByClassName('fi');

    for(let index=0; index < elements.length; index++ ) {     
			let	element = elements[index],
					elTop = element.getBoundingClientRect().top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
					$el = $(element),
					animateStartTop = elTop + element.offsetHeight/2;
				
			let subscribe = scrollStream
				.map((event) => {
					return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
				})
				.subscribe((scrollTop) => {
					let bottomOfWindow = scrollTop +  window.innerHeight;
					if( bottomOfWindow > animateStartTop){
						$el.addClass('fi_active');
						unsubscribe();
					}
				});
			let unsubscribe = () => {
				subscribe.unsubscribe();
			}
		}
}