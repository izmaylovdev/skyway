import { Observable } from 'rxjs/Observable';
import "rxjs/add/observable/fromEvent";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";

import SmoothScroll from 'smoothscroll-for-websites';

window.Observable = Observable;

import stickyFade from './modules/stickyFade';
import fadeSlideIn from './modules/fadeSlideIn';
import fadeIn from './modules/fadeIn';
import slideUp from './modules/SlideUp';
import hoverSlide from './modules/hoverSlide';
import localization from './modules/localization';
import progressResize from './modules/progressResize';
import carousel from './modules/carousel';
import bigCarousel from './modules/bigCarousel';
import nav from './modules/nav';
import slickInit from './modules/slickInit';
import namedPohotosHover from './modules/namedPhotos';
import showMoreButton from './modules/showMoreButton';
import tabs from './modules/tabs';
import tooltips from './modules/tooltips';
import tooltip from './modules/tooltip';
import radioGroup from './modules/radioGroup';
import heads from './modules/heads';
import initPopup from './modules/initPopup';
import youtubeVideo from './modules/youtubeVideo';

const resizeStream = Observable.fromEvent(window, 'resize');
const scrollStream = Observable.fromEvent(window, 'scroll')
			.map((event) => {
				var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
        return  scrollTop +  window.innerHeight;
			});
carousel(resizeStream);
radioGroup();
localization();
hoverSlide();
tooltips();
nav(resizeStream);

function init() {
	SmoothScroll({
		stepSize: 40,
		animationTime: 1000
	});

	initPopup();
	heads(scrollStream);
	showMoreButton();		
	slickInit();
	bigCarousel(resizeStream, scrollStream);
	tabs(resizeStream);
	stickyFade(scrollStream);
	namedPohotosHover();
	tooltip(scrollStream);
	fadeIn(scrollStream);
	fadeSlideIn(scrollStream);
	slideUp(scrollStream);
	if( $('.progress').length ){
		progressResize(resizeStream);
	}
	

	$("html, body").animate({scrollTop: window.scrollY+1},10);
}
window.onload = event => {

	init();
	document.body.style.display='none';
	setTimeout(function(){document.body.style.display='block'}, 50);
	document.getElementById('pagePreloader').style.display = 'none';
	youtubeVideo();

}

window.initMap = function (){
	var uluru = {lat: 45, lng: 35};
	var map = new google.maps.Map(document.getElementById('gmap'), {
		zoom: 2,
		center: uluru,
		scrollwheel: false
	});
	let arr = $('#gmap').data('points');
	var icon = new google.maps.MarkerImage(
					'pictures/mark.svg',
					null,
					new google.maps.Point(0, 0),
					new google.maps.Point(10, 10)
			);
	arr.forEach((el, index, arr) => {
		let position = { lat: el[0], lng: el[1] };
		new google.maps.Marker({
			position: position,
			map: map,
			icon: icon
		}) 
	});
}
$('.park__item').on('touchstart', function (){
	$(this).addClass('.park__item_active');

	$('body').one('touchstart',function (){
		$(this).removeClass('.park__item_active');
	});
})